package fr.lechfeck.texttoride;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;


public class PlayerAdapter extends BaseAdapter {

	private final static int MAX_PLAYERS = 5;
	
	private final Context mContext;
	private final ArrayList<Player> mPlayers = new ArrayList<Player>(MAX_PLAYERS);
	
	public PlayerAdapter(Context context) {
		this.mContext = context;
	}
	
	
	public void add(Player p) {
		mPlayers.add(p);
	}
	
	public boolean isSelected(int position) {
		return mPlayers.get(position).selected;
	}
	
	@Override
	public int getCount() {
		return mPlayers.size();
	}

	@Override
	public Object getItem(int position) {
		return mPlayers.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		View listItem;
		
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(mContext);
			listItem = inflater.inflate(R.layout.player_listitem, null);
		} else {
			listItem = convertView;
		}

		final Player player = mPlayers.get(position);
		
		TextView name = (TextView) listItem.findViewById(R.id.player_name_text);
		name.setText(player.name);
		
		final CheckBox cb = (CheckBox) listItem.findViewById(R.id.player_check);
		cb.setChecked(player.selected);
		cb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				player.selected = cb.isChecked();
			}
		});
		
		ImageButton removeButton = (ImageButton) listItem.findViewById(R.id.remove_player);
		removeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPlayers.remove(position);
				notifyDataSetChanged();
			}
		});

		return listItem;
	}
}
