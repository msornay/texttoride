package fr.lechfeck.texttoride;

class Player {
	String name;
	String phoneNumber;
	Boolean selected;
	
	Player(String name, String phoneNumber) {
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.selected = true;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
