package fr.lechfeck.texttoride;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import android.util.Log;

public class TtRRoutes {

	private static class Route {
		int src;
		int dst;
		int len;
		
		Route() {
			this.src = 0;
			this.dst = 0;
			this.len = 0;
		}
		
		Route(String[] array) {
			this.src = Integer.parseInt(array[0]);
			this.dst = Integer.parseInt(array[1]);
			this.len = Integer.parseInt(array[2]);
		}
	}
	
	public static TtRRoutes loadRoutes(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		
		
		if ((line = br.readLine()) == null) {
			throw new IOException("Malformed TtR routes file");
		}
		int numberOfTown = Integer.parseInt(line);
		
		ArrayList<String> towns = new ArrayList<String>();
		for (int i=0; i<numberOfTown; i++) {
			if ((line = br.readLine()) == null) {
				throw new IOException("Malformed TtR routes file");
			}			
			towns.add(line);
		}
		
		TreeSet<Route> routes = new TreeSet<Route>(new Comparator<Route>() {
			@Override
			public int compare(Route lhs, Route rhs) {
				int lenDiff = rhs.len - lhs.len;
				if (lenDiff != 0) {
					return lenDiff;
				}
				
				int srcDiff = lhs.src - rhs.src;
				if (srcDiff != 0) {
					return srcDiff;
				}
				
				return lhs.dst - rhs.dst;
			}
		});
		
		while ((line = br.readLine()) != null) {
			Route r = new Route(line.split(" "));
			routes.add(r);
		}
		
		Log.d("loadRoutes", "Loaded "+towns.size()+" towns");
		Log.d("loadRoutes", "Loaded "+routes.size()+" routes");
		
		return new TtRRoutes(towns, routes);
	}

	private final ArrayList<String> mTowns;
	private final TreeSet<Route> mRoutes;
	private final Random mRand;
		
	private TtRRoutes(ArrayList<String> towns, TreeSet<Route> routes) {
		this.mTowns = towns;
		this.mRoutes = routes;
		this.mRand = new Random();
	}
	
	public int getMinLength() {
		return mRoutes.last().len;
	}
	
	public int getMaxLength() {
		return mRoutes.first().len;
	}
	
	private final StringBuilder routeStringBuilder = new StringBuilder();
	private String routeString(Route r) {
		routeStringBuilder.setLength(0);
		routeStringBuilder.append(mTowns.get(r.src));
		routeStringBuilder.append(" ");
		routeStringBuilder.append(mTowns.get(r.dst));
		routeStringBuilder.append(" (");
		routeStringBuilder.append(r.len);
		routeStringBuilder.append(")");
		return routeStringBuilder.toString();
	}
	
	private final ArrayList<Route> mToChooseFrom = new ArrayList<Route>();
	private final Route dummyRouteMin = new Route();
	private final Route dummyRouteMax = new Route();
	public String[] getRandomRoute(int n, int min, int max) {
		dummyRouteMin.len = min;
		dummyRouteMax.len = max;
		
		SortedSet<Route> set = mRoutes.subSet(dummyRouteMax, true, dummyRouteMin, true);
		
		if (!mToChooseFrom.isEmpty()) {
			mToChooseFrom.clear();
		}
		mToChooseFrom.addAll(set);

		if (n > mToChooseFrom.size()) {
			return null;
		}
		
		String[] sample = new String[n];
		for (int i=0; i<n; i++) {
			int k = mRand.nextInt(mToChooseFrom.size());
			Route r = mToChooseFrom.get(k);
			sample[i] = routeString(r);
			mToChooseFrom.remove(k);
		}
		
		return sample;
	}

}
