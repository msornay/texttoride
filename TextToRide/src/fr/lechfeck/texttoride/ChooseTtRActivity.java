package fr.lechfeck.texttoride;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ChooseTtRActivity extends Activity {

    static final String EXTRA_EDITION_ASSET = "extraEditionAsset";
    static String TTR_EDITION_FOLDER = "ttr_editions";

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_tt_r);
        
        final ListView lv = (ListView) findViewById(R.id.ttr_edition_list);
        try {
			lv.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
					getAssets().list(TTR_EDITION_FOLDER)));
		} catch (IOException e) {
			throw new RuntimeException("Error while processing assets folder");
		}
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String asset = (String) lv.getAdapter().getItem(position);
				Log.d("ChooseTtR: Click on list item :", asset);
				
				Intent intent = new Intent(ChooseTtRActivity.this, GenerateRoutesActivity.class);
				intent.putExtra(EXTRA_EDITION_ASSET, asset);
				Log.d("ChooseTtR: Starting new activity for edition:", asset);
				startActivity(intent);
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.choose_tt_r, menu);
        return true;        
    }
    
}
