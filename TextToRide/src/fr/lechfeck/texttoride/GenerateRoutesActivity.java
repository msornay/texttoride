package fr.lechfeck.texttoride;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class GenerateRoutesActivity extends Activity {

	private PlayerAdapter mPlayers;
	private TtRRoutes mAllRoutes;
	
	private static final int PICK_CONTACT_REQUEST = 0;
	private static final String SMS_SENT = "SMS_SENT";
	
	private static final int MAX_SMS_PER_PLAYER = 100;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_generate_routes);
		
		Intent intent = getIntent();
		String assetName = intent.getStringExtra(ChooseTtRActivity.EXTRA_EDITION_ASSET);
		try {
			mAllRoutes = TtRRoutes.loadRoutes(getAssets().open(ChooseTtRActivity.TTR_EDITION_FOLDER+"/"+assetName));
		} catch (IOException e) {
			throw new RuntimeException(e); 
		}
		
		final int routeMin = mAllRoutes.getMinLength();
		final int routeMax = mAllRoutes.getMaxLength();
		
		final Spinner minSpinner = (Spinner) findViewById(R.id.min_len_spin);
		minSpinner.setAdapter(new RangeAdapter(getLayoutInflater(), routeMin, routeMax+1));
		
		final Spinner maxSpinner = (Spinner) findViewById(R.id.max_len_spin);
		maxSpinner.setAdapter(new RangeAdapter(getLayoutInflater(), routeMin, routeMax+1));
		maxSpinner.setSelection(maxSpinner.getAdapter().getCount()-1);
		
		minSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				RangeAdapter maxAdapter = (RangeAdapter)maxSpinner.getAdapter();
				maxAdapter.mMin = routeMin+position;
				maxAdapter.notifyDataSetChanged();
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// do nothing		
			}
		});
		
		final EditText numberText = (EditText) findViewById(R.id.nroutes_edit);
		numberText.setText("1");
		
		mPlayers = new PlayerAdapter(this);
		
		final ListView lv = (ListView) findViewById(R.id.player_list);
		lv.setAdapter(mPlayers);
				
		Button contactButton = (Button) findViewById(R.id.add_contact_button);
		contactButton.setOnClickListener(new View.OnClickListener() {	
			@Override
			public void onClick(View v) {
				Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, 
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
				startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST);		
			}
		});
		
		final Button sendButton = (Button) findViewById(R.id.send_sms_button);
		sendButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
							
				int nRoutesPerPlayer = Integer.parseInt(numberText.getText().toString());
				
				if (nRoutesPerPlayer <= 0) {
					return;
				}
				
				if (nRoutesPerPlayer > MAX_SMS_PER_PLAYER) {
					Toast.makeText(GenerateRoutesActivity.this, 
							"Maximum number of routes per player is 100", // TODO : @string
							Toast.LENGTH_SHORT).show();
					return;
				}
				
				ArrayList<Player> activePlayers = new ArrayList<Player>();
				for (int i=0; i<mPlayers.getCount(); i++) {
					if (mPlayers.isSelected(i)) {
						activePlayers.add((Player)mPlayers.getItem(i));
					}
				}
				
				if (activePlayers.size() == 0) {
					return;
				}
				
				int min = (Integer) minSpinner.getSelectedItem();
				int max = (Integer) maxSpinner.getSelectedItem();
			
				Log.d("Click on generate button", "["+min+" "+" "+max+"]");
				String[] routes = mAllRoutes.getRandomRoute(activePlayers.size()*nRoutesPerPlayer, min, max);
				if (routes == null) {
					Toast.makeText(GenerateRoutesActivity.this, "No enough routes found for your request", // TODO : @string
							Toast.LENGTH_SHORT).show();
					return;
				}
				
				
				final ArrayList<SmsMessage> messages = new ArrayList<SmsMessage>(activePlayers.size());
				for (int i=0; i<activePlayers.size(); i++) {
					boolean last = false;
					if (i == activePlayers.size()-1) {
						last = true;
						Log.d("Sending sms", "Sending last sms");
					}				
					SmsMessage sms = makeSms(activePlayers.get(i), routes, 
							i*nRoutesPerPlayer, nRoutesPerPlayer, last);
					messages.add(sms);
				}
				
				int n = 0;
				for (SmsMessage sms : messages) {
					n += sms.message.size();
				}
				
				new AlertDialog.Builder(GenerateRoutesActivity.this)
				.setTitle(R.string.sure)
				.setMessage(getString(R.string.number_sms, n))
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						sendButton.setText(getString(R.string.sending));
						sendButton.setEnabled(false);
						
						for (SmsMessage sms : messages) {
							mSmsManager.sendMultipartTextMessage(sms.phoneNumber, null, 
									sms.message, sms.intents, null);
						}		
					}

				})
				.setNegativeButton(R.string.no, null)
				.show();
			}
		});

		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				int resCode = getResultCode();
				if (resCode != Activity.RESULT_OK) {
					Toast.makeText(GenerateRoutesActivity.this, 
							"Error: Text message not sent", Toast.LENGTH_SHORT).show();
					Log.e("Error while sending sms:", ""+resCode);
				}
				Log.d("Broadcast receiver:", "Received last sms sent notification");
				sendButton.setEnabled(true);
				sendButton.setText(getString(R.string.send_sms));
			}
		}, new IntentFilter(SMS_SENT));
		
	}
	
	StringBuilder mSmsText = new StringBuilder();
	SmsManager mSmsManager = SmsManager.getDefault();
	private SmsMessage makeSms(Player player, String[] routes, int offset, int n, boolean last) {
		mSmsText.setLength(0);
		mSmsText.append(player.name);
		mSmsText.append("\n");
		for (int i = offset; i < offset + n; i++) {
			mSmsText.append(routes[i]);
			if (i != offset + n - 1 ) {
				mSmsText.append("\n");
			}
		}
		
		Log.d("Sending sms to:", player.name+ " : "+player.phoneNumber);
		Log.d("Content:", mSmsText.toString());
		
		ArrayList<String> dividedMessage = mSmsManager.divideMessage(mSmsText.toString());
		
		ArrayList<PendingIntent> pis = null;
		if (last) {
			pis = new ArrayList<PendingIntent>(dividedMessage.size());
			for (int i=0; i<dividedMessage.size(); i++) {
				pis.add(null);
			}
			// Broadcast intent only for the last part.
			pis.set(dividedMessage.size()-1, 
					PendingIntent.getBroadcast(this, 0, new Intent(SMS_SENT), 0));
		}
		
		return new SmsMessage(player.phoneNumber, dividedMessage, pis);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			return;
		}
		
		if (requestCode == PICK_CONTACT_REQUEST) {	
			Uri contactData = data.getData();
			Cursor c = getContentResolver().query(contactData, null, null, null, null);
			if (c.moveToFirst()) {
				String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				String phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				Log.d("Contact picked name:", name);
				Log.d("Contact picked phone:", phone);
				
				Player p = new Player(name, phone);
				mPlayers.add(p);
				mPlayers.notifyDataSetChanged();
				
			} else {
				Log.e("Contact picker result:", "error while retrieving contact info");
			}
			c.close();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.generate_routes, menu);
		return true;
	}

}
