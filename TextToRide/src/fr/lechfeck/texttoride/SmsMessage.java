package fr.lechfeck.texttoride;

import java.util.ArrayList;

import android.app.PendingIntent;

public class SmsMessage {

	final String phoneNumber;
	final ArrayList<String> message;
	final ArrayList<PendingIntent> intents;

	public SmsMessage(String phoneNumber, 
			ArrayList<String> message, ArrayList<PendingIntent> intents) {
		this.phoneNumber = phoneNumber;
		this.message = message;
		this.intents = intents;
	}
}
