package fr.lechfeck.texttoride;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


class RangeAdapter extends BaseAdapter {
	private final LayoutInflater mInflater;
	int mMin;
	int mMax;
	
	public RangeAdapter(LayoutInflater inflater, int min, int max) {
		this.mInflater = inflater;
		this.mMin = min;
		this.mMax = max;
	}
	
	@Override
	public int getCount() {
		return mMax-mMin;
	}

	@Override
	public Object getItem(int position) {
		return mMin+position;
	}

	@Override
	public long getItemId(int position) {
		return mMin+position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView view;
		if (convertView == null) {
			view = (TextView) mInflater.inflate(android.R.layout.simple_spinner_item, parent, false);
		} else {
			view = (TextView) convertView;
		}
		view.setText(getItem(position).toString());
		return view;		
	}
	
}
