import gen_routes

import unittest

import pprint

class TestMap(unittest.TestCase):
    def setUp(self):
        self.map = gen_routes.Map()

    def test_getset_route(self):
        self.map['Paris', 'Brest'] = 3
        
        # The entry we have set
        self.assertEqual(self.map['Paris', 'Brest'], 3)

        # The reverse
        self.assertEqual(self.map['Brest', 'Paris'], 3)

        # Towns have been added
        self.assertEqual(self.map['Brest', 'Brest'], 0)
        self.assertEqual(self.map['Paris', 'Paris'], 0)

    def test_length(self):
        self.map['Paris', 'Le Mans'] = 2
        self.map['Brest', 'Le Mans'] = 1

        self.assertEqual(self.map['Paris', 'Brest'], 3)

    def test_routes(self):
        self.map['Paris', 'Le Mans'] = 2
        self.map['Brest', 'Le Mans'] = 1

        r = self.map.routes()
        self.assertEqual(r[0], ('Brest', 'Paris', 3))
        
if __name__ == '__main__':
    unittest.main()
