""" gen_routes.py -- generate all routes for a TtR map, sorted by length.

"""

import fileinput
import itertools

import bencode

class Map(object):
    """ Map objects represents a TtR board.
    Example usage :

    >>> m = Map()
    >>> map['Paris', 'Le Mans'] = 2
    >>> map['Brest', 'Le Mans'] = 1
    >>> map['Paris', 'Brest'] == 3
    True
    
    """    
    def __init__(self):
        self.towns = set()
        self._graph = {}

        self._uptodate = True
        self._updating = False
        
    @staticmethod
    def _key(src, dst):
        if src > dst:
            src, dst = dst, src
        return (src, dst)

    def _add_town(self, t):
        if t not in self.towns:
            self.towns.add(t)
            self._graph[(t, t)] = 0
    
    def __getitem__(self, (src, dst)):
        if not self._updating and not self._uptodate:
            self._update()
            
        k = Map._key(src, dst)
        return self._graph.get(k, float('inf'))

    def __setitem__(self, (src, dst), value):
        self._add_town(src)
        self._add_town(dst)
            
        k = Map._key(src, dst)
        self._graph[k] = value

        self._uptodate = False
        
    def _update(self):
        self._updating = True
        # Floyd-Warshall
        for k,i,j in itertools.product(self.towns, repeat=3):
            if self[i,j] > self[i,k] + self[k,j]:
                self[i,j] = self[i,k] + self[k,j]
        self._updating = False
        self._uptodate = True

    def routes(self):
        """ Return the list of all routes, sorted by decreasing lengths.
        """
        self._update()
        r = [(i, j, self[i, j]) for i, j in self._graph]
        r.sort(key=lambda (src, dst, length): length, reverse=True)
        return filter(lambda (src, dst, length): length > 0, r)

            
if __name__ == '__main__':

    m = Map()
    for line in fileinput.input():
        src, dst, length = line.split(' ')
        m[src, dst] = int(length)

    # Output : Every routes

    # Print the list of all towns
    towns = [t for t in m.towns]
    print len(towns)
    for t in towns:
        print t
    # Print 3 ints per line: 'town_index town_index length'
    # Indexes refer to the towns list printed before
    for src, dst, length in m.routes():
        print towns.index(src), towns.index(dst), length
    
